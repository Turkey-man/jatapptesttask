//
//  MainCoordinator.swift
//  NavigationTask
//
//  Created by Herr Truthann on 31.01.2021.
//

import UIKit

enum StoryboardNames {
    case login
    case list
}

class MainCoordinator: Coordinator {
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        DispatchQueue.main.async {
            let vc = SignupViewController.instantiate(storyboardName: StoryboardNames.login.path)
            vc.coordinator = self
            self.navigationController.pushViewController(vc, animated: true)
        }
    }
    
    func pushLoginScreen() {
        DispatchQueue.main.async {
            let vc = LoginViewController.instantiate(storyboardName: StoryboardNames.login.path)
            vc.coordinator = self
            self.navigationController.pushViewController(vc, animated: true)
        }
    }
    
    func pushListScreen() {
        DispatchQueue.main.async {
            let vc = ListViewController.instantiate(storyboardName: StoryboardNames.list.path)
//        vc.coordinator = self
            self.navigationController.pushViewController(vc, animated: true)
        }
    }
}

extension StoryboardNames {
    var path: String {
        switch self {
        case .login:
            return "Login"
        case .list:
            return "List"
        }
    }
}
