//
//  Coordinator.swift
//  NavigationTask
//
//  Created by Herr Truthann on 31.01.2021.
//

import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    
    func start()
}
