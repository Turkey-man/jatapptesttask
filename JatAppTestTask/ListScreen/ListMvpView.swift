//
//  ListMvpView.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 25.01.2021.
//

import Foundation

protocol ListMvpView: class {
    func reloadData()
    func showNoConnectionAlert()
}
