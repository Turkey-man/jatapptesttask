//
//  ListViewController.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 25.01.2021.
//

import UIKit

final class ListViewController: UIViewController, Storyboarded {

    @IBOutlet private var tableView: UITableView!
    
    private var presenter: ListPresenter?
    private let cellIdentifier: String = "cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter = ListPresenter(view: self)
        setupTableView()
        presenter?.onStart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
    }
}

extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.dictArray.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        cell.backgroundColor = .white
        cell.textLabel?.text = presenter?.dictArray[indexPath.row].description
        cell.textLabel?.textColor = .black
        return cell
    }
}

extension ListViewController: ListMvpView {
    func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func showNoConnectionAlert() {
        Alerts.showNoConnectionAlert(vc: self)
    }
}
