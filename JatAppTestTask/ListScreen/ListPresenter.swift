//
//  ListPresenter.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 25.01.2021.
//


import Foundation

final class ListPresenter {
    
    private weak var view: ListMvpView?
    private let network = Network()
    private let model = Model()
    private var charactersDictionary = [Character: Int]()
    private var text = ""
    var dictArray = [[Character: Int]]()
    
    init(view: ListMvpView) {
        self.view = view
    }
    
    func onStart() {
        if network.checkConnection() {
            guard let token = model.getToken() else { return }
            network.getText(token: token) { [weak self] (result: Result<TextModel, Error>) in
                if let textModel = try? result.get() {
                    if let text = textModel.data {
                        self?.text = text
                        self?.countOccurences()
                        self?.view?.reloadData()
                    }
                }
            }
        } else {
            view?.showNoConnectionAlert()
        }
    }
    
    private func countOccurences() {
        let dictionary = text.reduce([:]) { dictionary, character -> [Character: Int] in
            var finalDictionary = dictionary
            let count = finalDictionary[character] ?? 0
            finalDictionary[character] = count + 1
            return finalDictionary
        }
        for (char, count) in dictionary {
            dictArray.append([char: Int(count)])
        }
    }
}
