//
//  SignupMvpView.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 26.01.2021.
//

import Foundation

protocol SignupMvpView: class {
    func pushToListScreen()
    func showNoConnectionAlert()
    func showSignupFailureAlert()
}
