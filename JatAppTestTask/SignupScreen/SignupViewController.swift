//
//  SignupViewController.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 26.01.2021.
//

import Combine
import UIKit

final class SignupViewController: UIViewController, Storyboarded {

    @IBOutlet private var nameTextField: UITextField!
    @IBOutlet private var emailTextField: UITextField!
    @IBOutlet private var passwordTextField: UITextField!
    @IBOutlet private var nameErrorLabel: UILabel!
    @IBOutlet private var emailErrorLabel: UILabel!
    @IBOutlet private var passwordErrorLabel: UILabel!
    @IBOutlet private var signupButton: UIButton!
    
    private var nameDataSubject = CurrentValueSubject<String, Never>("")
    private var emailDataSubject = CurrentValueSubject<String, Never>("")
    private var passwordDataSubject = CurrentValueSubject<String, Never>("")
    @Published private var nameIsValid = false
    @Published private var emailIsValid = false
    @Published private var passwordIsValid = false
    private var subscriptions = Set<AnyCancellable>()
    private var presenter: SignupPresenter?
    weak var coordinator: MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addKeyboardNotifications()
        setupSubscriptions()
        presenter = SignupPresenter(view: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        clearTextFields()
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    private func setupSubscriptions() {
        subscribeWithElements(subject: nameDataSubject, isValid: \SignupViewController.nameIsValid, errorLabel: nameErrorLabel, textField: nameTextField)
        subscribeWithElements(subject: emailDataSubject, isValid: \SignupViewController.emailIsValid, errorLabel: emailErrorLabel, textField: emailTextField)
        subscribeWithElements(subject: passwordDataSubject, isValid: \SignupViewController.passwordIsValid, errorLabel: passwordErrorLabel, textField: passwordTextField)
        Publishers.CombineLatest3($nameIsValid, $emailIsValid, $passwordIsValid)
            .map { $0 && $1 && $2 }
            .sink { canSignUp in
                self.signupButton.isEnabled = canSignUp
            }.store(in: &subscriptions)
    }
    
    private func subscribeWithElements(subject: CurrentValueSubject<String, Never>, isValid: WritableKeyPath<SignupViewController, Bool>, errorLabel: UILabel, textField: UITextField) {
        subject
            .debounce(for: 0.8, scheduler: RunLoop.main)
            .map { $0.count >= 6 }
            .sink { [weak self] valid in
                self?[keyPath: isValid] = valid
                errorLabel.isHidden = valid
                if textField.text == "" {
                    errorLabel.isHidden = true
                }
        }.store(in: &subscriptions)
    }
    
    private func clearTextFields() {
        nameTextField.text = ""
        emailTextField.text = ""
        passwordTextField.text = ""
    }
    
    @IBAction func nameChanged(_ sender: UITextField) {
        let enteredName = sender.text ?? ""
        nameDataSubject.send(enteredName)
    }
    
    @IBAction func emailChanged(_ sender: UITextField) {
        let enteredEmail = sender.text ?? ""
        emailDataSubject.send(enteredEmail)
    }
    
    @IBAction func passwordChanged(_ sender: UITextField) {
            let enteredPassword = sender.text ?? ""
            passwordDataSubject.send(enteredPassword)
    }
    
    @IBAction func signupButtonTapped(_ sender: Any) {
        guard let name = nameTextField.text, let email = emailTextField.text, let password = passwordTextField.text else { return }
        presenter?.signup(name: name, email: email, password: password)
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        coordinator?.pushLoginScreen()
    }
}

extension SignupViewController: SignupMvpView {
    func pushToListScreen() {
        coordinator?.pushListScreen()
    }
    
    func showNoConnectionAlert() {
        Alerts.showNoConnectionAlert(vc: self)
    }
    
    func showSignupFailureAlert() {
        Alerts.showSignupFailureAlert(vc: self)
    }
}
