//
//  SignupPresenter.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 26.01.2021.
//

import Foundation

final class SignupPresenter {
    
    private weak var view: SignupMvpView?
    private let model = Model()
    private let network = Network()
    
    init(view: SignupMvpView) {
        self.view = view
    }
    
    func signup(name: String, email: String, password: String) {
        if network.checkConnection() {
            network.authenticateUser(isLogin: false, name: name, email: email, password: password) { [weak self] (result: Result<SignupModel, Error>) in
                if let signupModel = try? result.get() {
                    if let accessToken = signupModel.data?.accessToken {
                        self?.model.saveToken(token: accessToken)
                        self?.view?.pushToListScreen()
                    } else {
                        self?.view?.showSignupFailureAlert()
                    }
                }
            }
        } else {
            view?.showNoConnectionAlert()
        }
    }
}
