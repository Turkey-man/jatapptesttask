//
//  Network.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 25.01.2021.
//

import Foundation

final class Network {
    
    private let session = URLSession.shared
    private let baseUrl = Links.baseURL.rawValue
    
    func authenticateUser<T: Decodable>(isLogin: Bool, name: String?, email: String, password: String, completion: @escaping (Result<T, Error>) -> ()) {
        guard let completeUrl = URL(string: baseUrl + (isLogin ? Links.login.rawValue : Links.signup.rawValue)) else { return }
        var request = URLRequest(url: completeUrl)
        var headers: [String: Any] = ["email": email, "password": password]
        if !isLogin {
            headers["name"] = name
        }
        guard let httpBody = try? JSONSerialization.data(withJSONObject: headers, options: .prettyPrinted) else { return }
        request.httpBody = httpBody
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        session.dataTask(with: request) { data, response, error in
            data
                .map(self.parseJson(data:))
                .map(completion)
        }.resume()
    }
    
    func getText<T: Decodable>(token: String, completion: @escaping (Result<T, Error>) -> ()) {
        guard let completeUrl = URL(string: baseUrl + Links.getText.rawValue) else { return }
        var request = URLRequest(url: completeUrl)
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        print("Token used: \(token)")
        session.dataTask(with: request) { data, response, error in
            data
                .map(self.parseJson(data:))
                .map(completion)
        }.resume()
    }
    
    private func parseJson<T: Decodable>(data: Data) -> Result<T, Error> {
            return Result(catching: {
                try JSONDecoder().decode(T.self, from: data)
            })
        }
    
    func checkConnection() -> Bool {
        let connection = CheckInternetConnection.connectionCheck()
        return connection
    }
}
