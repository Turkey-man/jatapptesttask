//
//  Alerts.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 25.01.2021.
//

import UIKit

struct Alerts {
    private static func showBasicAlert(vc: UIViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        DispatchQueue.main.async {
            vc.present(alert, animated: true)
        }
    }
    
    static func showNoConnectionAlert(vc: UIViewController) {
        showBasicAlert(vc: vc, title: "No connection", message: "No Internet connection!")
    }
    
    static func showSignupFailureAlert(vc: UIViewController) {
        showBasicAlert(vc: vc, title: "Signup Failure", message: "Failed to sign up!")
    }
    
    static func showLoginFailureAlert(vc: UIViewController) {
        showBasicAlert(vc: vc, title: "Login Failure", message: "Failed to log in!")
    }
}
