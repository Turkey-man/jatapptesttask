//
//  KeyboardExtension.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 27.01.2021.
//

import UIKit

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                         action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }

    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let height = UIScreen.main.bounds.height
        let width = UIScreen.main.bounds.width
        self.view.frame = CGRect(x: 0, y: 0, width: width, height: height)
        self.view.layoutIfNeeded()
    }
    
    func addKeyboardNotifications() {
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(keyboardWillHide(notification:)),
                         name: UIResponder.keyboardWillHideNotification,
                         object: nil)
        
        hideKeyboardWhenTappedAround()
    }
}
