//
//  Links.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 25.01.2021.
//

import Foundation

enum Links: String {
    case baseURL = "https://apiecho.cf/api/"
    case getText = "get/text/"
    case signup = "signup/"
    case login = "login/"
}
