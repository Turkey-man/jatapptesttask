//
//  RegExExtension.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 26.01.2021.
//

import Foundation

extension String {
    public func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regEx = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regEx.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}
