//
//  SignupModel.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 25.01.2021.
//

import Foundation

struct SignupModel: Codable {
    let success: Bool?
    let data: SignupDataModel?
}

struct SignupDataModel: Codable {
    let accessToken: String?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
    }
}
