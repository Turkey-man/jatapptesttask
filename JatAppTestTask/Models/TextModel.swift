//
//  TextModel.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 25.01.2021.
//

import Foundation

struct TextModel: Codable {
    let data: String?
}
