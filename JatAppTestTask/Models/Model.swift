//
//  Model.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 25.01.2021.
//

import Foundation

final class Model {
    
    private let defaults = UserDefaults.standard
   
    func saveToken(token: String) {
        defaults.set(token, forKey: "token")
    }
    
    func getToken() -> String? {
        let token = defaults.string(forKey: "token")
        return token
    }
}
