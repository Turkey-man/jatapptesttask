//
//  LoginViewController.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 25.01.2021.
//

import Combine
import UIKit

final class LoginViewController: UIViewController, Storyboarded {
    
    @IBOutlet private var emailTextField: UITextField!
    @IBOutlet private var passwordTextField: UITextField!
    @IBOutlet private var emailErrorLabel: UILabel!
    @IBOutlet private var passwordErrorLabel: UILabel!
    @IBOutlet private var loginButton: UIButton!
    
    private var emailDataSubject = CurrentValueSubject<String, Never>("")
    private var passwordDataSubject = CurrentValueSubject<String, Never>("")
    @Published private var emailIsValid = false
    @Published private var passwordIsValid = false
    private var subscriptions = Set<AnyCancellable>()
    private var presenter: LoginPresenter?
    weak var coordinator: MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addKeyboardNotifications()
        presenter = LoginPresenter(view: self)
        setupSubscriptions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    private func setupSubscriptions() {
        subscribeWithElements(subject: emailDataSubject, isValid: \LoginViewController.emailIsValid, errorLabel: emailErrorLabel, textField: emailTextField)
        subscribeWithElements(subject: passwordDataSubject, isValid: \LoginViewController.passwordIsValid, errorLabel: passwordErrorLabel, textField: passwordTextField)
        Publishers.CombineLatest($emailIsValid, $passwordIsValid)
            .map { $0 && $1 }
            .sink { [weak self] canLogin in
                self?.loginButton.isEnabled = canLogin
            }.store(in: &subscriptions)
    }
    
    private func subscribeWithElements(subject: CurrentValueSubject<String, Never>, isValid: WritableKeyPath<LoginViewController, Bool>, errorLabel: UILabel, textField: UITextField) {
        subject
            .debounce(for: 0.8, scheduler: RunLoop.main)
            .map { $0.count >= 6 }
            .sink { [weak self] valid in
                self?[keyPath: isValid] = valid
                errorLabel.isHidden = valid
                if textField.text == "" {
                    errorLabel.isHidden = true
                }
        }.store(in: &subscriptions)
    }
    
    @IBAction func emailChanged(_ sender: UITextField) {
        let enteredEmail = sender.text ?? ""
        emailDataSubject.send(enteredEmail)
    }
    
    @IBAction func passwordChanged(_ sender: UITextField) {
        let enteredPassword = sender.text ?? ""
        passwordDataSubject.send(enteredPassword)
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        guard let email = emailTextField.text, let password = passwordTextField.text else { return }
        presenter?.login(email: email, password: password)
    }
}

extension LoginViewController: LoginMvpView {
    func pushToListScreen() {
        coordinator?.pushListScreen()
    }
    
    func showNoConnectionAlert() {
        Alerts.showNoConnectionAlert(vc: self)
    }
    
    func showLoginFailureAlert() {
        Alerts.showLoginFailureAlert(vc: self)
    }
}
