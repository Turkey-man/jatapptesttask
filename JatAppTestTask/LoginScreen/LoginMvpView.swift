//
//  LoginMvpView.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 25.01.2021.
//

import Foundation

protocol LoginMvpView: class {
    func pushToListScreen()
    func showNoConnectionAlert()
    func showLoginFailureAlert()
}
