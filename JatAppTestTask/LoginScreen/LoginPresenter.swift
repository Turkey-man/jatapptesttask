//
//  LoginPresenter.swift
//  JatAppTestTask
//
//  Created by Herr Truthann on 25.01.2021.
//

import Foundation

final class LoginPresenter {
    
    private weak var view: LoginMvpView?
    private let model = Model()
    private let network = Network()
    
    init(view: LoginMvpView) {
        self.view = view
    }
    
    func login(email: String, password: String) {
        if network.checkConnection() {
            network.authenticateUser(isLogin: true, name: nil, email: email, password: password) { [weak self] (result: Result<LoginModel, Error>) in
                if let loginModel = try? result.get() {
                    if let success = loginModel.success {
                        if success {
                            self?.view?.pushToListScreen()
                        } else {
                            self?.view?.showLoginFailureAlert()
                        }
                    } else {
                        self?.view?.showLoginFailureAlert()
                    }
                }
            }
        } else {
            view?.showNoConnectionAlert()
        }
    }
}
